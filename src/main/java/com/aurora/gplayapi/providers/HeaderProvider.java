package com.aurora.gplayapi.providers;

import com.aurora.gplayapi.modals.ApiBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class HeaderProvider {

    public static Map<String, String> getAuthHeaders(ApiBuilder builder) {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("User-Agent", builder.getDeviceInfoProvider().getAuthUserAgentString());
        if (StringUtils.isNotEmpty(builder.getGsfId()))
            headers.put("device", builder.getGsfId());
        return headers;
    }

    public static Map<String, String> getDefaultHeaders(ApiBuilder builder) {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer " + builder.getAuthToken());
        headers.put("User-Agent", builder.getDeviceInfoProvider().getUserAgentString());
        headers.put("X-DFE-Device-Id", builder.getGsfId());
        headers.put("Accept-Language", builder.getLocale().toString().replace("_", "-"));
        headers.put("X-DFE-Encoded-Targets", "CAESpwJX6pSBBqYK0QJCAtgDAQEUkgeAAqQItQFYQJkBuwQykgHpCpgBugEyhgEvaPIC3QEn3AEW+wu4AwECzwWuA5oTNdEIvAHbELYBAaUDngMBLyjjC8MCowKtA7AC9AOvDbgC0wHfBlcBqgKbAssBUYMDF272AeUBTIgCGALlAQIUswEHYkJLYgHXCg2hBNwBQE4BYRP6AS1dMvMCogKAA80CtgGrBMgB3gQKwQGHAZMCYgnaAmwPiAJjMQEizQLmAYYCvgEB3QEOE7kBqgHEA9cCHAelAQHFAToBA/MBiQGOAQEH5QGWBANGAQYHCOsBygFXyQHlAQUcMbsCZ5sBlAKQAjjfAgElbI4KkwVwRYIBggc1kwE5KtAB1gN6jwU2RckBsQScAtENGqQHEQEBAQEBAskBHCvOAe0BAgMEawMEAS+A088CgruxAwEBAgMECQgJAQIIBAECAQYBAQQFBAgNBgIMAwMDAQ0BAQEFAQEBxgEBEgQEAg0mwQF9LwIcAQEKkAEMMxcBIQoUDwYHIjeEAQ4MFk0JWH8RERgBA4sBgQEUECMIEXBkEQ9fC6MBwAKEAQSIAYoBGRgLKxYWBQcBKmUCAiUocxQnLfQEMQ43GIUBjQG0AVlCjgEeMiQQJi9b1AFiA3cJAQowrgF5qgEMAyxkngEEgQF0UEXUAYoBzAIFBQnNAQQBRaABCDE4pwJgNS7OAQ1yqwEgiwM/+wImlwMeQ60ChAZ24wWCBAkE9gMWc5wBVW0BCTwB3gUgEA57VV6VAYYGKxjYAQEhAQcCIAgSHQemAzgBGkaEAQG7AnV3MBgBIgKjAhIBARgWD8YLHYABhwGEAsoBAQIBwwEn6wIBOQHbAVLnA0H1AsIBdQETKQSLAbIDSpsBBhI/RDgUK1VFU48CgwIKDgcvXBdSGrkBDvcBtwEqFAHSA98DlwEE6wGHAWIu0wEGExILWigkAQIChAW1AQ0GI1konwEyHhgBBCACVgEjApABHRIbJ36JAV0MD/0BIyYiBAEiKh6AAj8EGwMXIIoBUj2yAcoCCxixAiV+G1q7AQyIASV3iwGBAUcBKwU3AlQBYqQCITABDUUDngMdsQFxfxBmvQQL7AEHOIwBHgyNAwFxAQIVoAFragI6UQgCCYoEFBQCAwExMlMYAgPKAZkBOgEBBleEATumAgosyQEWWzZHiQEZOCYOXjIRNJ8BP0ZGvwIEKCZhERw/iQEcJVMGV5EBMgEKngLSAgQSTSUCjAGDARF1IDKQAgzKAQICAgcEAQQCBgQDBgUHBAIGBgQCBAIGBQICAgYEAwQe0wF+VTkhJB8oNgEBCCkBaTt0BAIEAQYEAwSyAbACJoQCBgcGBhUCKx0SBAoBbQYGAwICBjgIPg0JOGkbCJEBdw4NAz0uhAEGARGEAQ0hCAJE1wE8IcIBAYcBQQEJXR4eBgMWGitnKywePhcDRAgKjQEUPEsNXjk6BQcFBgcEAQYEAwVADiUEAQcGBgQfDIYBsgMpBTsCBQIKWSYHGv0BBQMJLg5YAiEJCk45FgIjCBUMIRoCJARXnAFCNwcEAQYGMFcbKnm5AhAJHgMKLy4ZBQMCAQIDAkMj1AEIqQMFBREJNheoAQurAQECJCGDARIyARFDBgYGBAM");
        headers.put("X-DFE-Client-Id", "am-android-google");
        headers.put("X-DFE-Network-Type", "4");
        headers.put("X-DFE-Content-Filters", "");
        headers.put("X-DFE-UserLanguages", builder.getLocale().toString());
        headers.put("X-DFE-Request-Params", "timeoutMs=30000");

        if (StringUtils.isNotEmpty(builder.getDeviceCheckInConsistencyToken())) {
            headers.put("X-DFE-Device-Checkin-Consistency-Token", builder.getDeviceCheckInConsistencyToken());
        }

        if (StringUtils.isNotEmpty(builder.getDeviceConfigToken())) {
            headers.put("X-DFE-Device-Config-Token", builder.getDeviceConfigToken());
        }

        if (StringUtils.isNotEmpty(builder.getDfeCookie())) {
            headers.put("X-DFE-Cookie", builder.getDfeCookie());
        }

        String mccMnc = builder.getDeviceInfoProvider().getMccMnc();
        if (StringUtils.isNotEmpty(mccMnc)) {
            headers.put("X-DFE-MCCMNC", mccMnc);
        }
        return headers;
    }

    public static Map<String, String> getAASTokenHeaders(String email, String oauthToken) {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("service", "ac2dm");
        headers.put("add_account", "1");
        headers.put("get_accountid", "1");
        headers.put("ACCESS_TOKEN", "1");
        headers.put("callerPkg", "com.google.android.gms");
        headers.put("Token", oauthToken);
        return headers;
    }
}
