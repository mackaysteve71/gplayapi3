package com.aurora.gplayapi.retro;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.Map;

public interface RetroService {

    @POST()
    Call<ResponseBody> post(
            @Url String url,
            @HeaderMap Map<String, String> headers,
            @Body RequestBody body
    );

    @GET()
    Call<ResponseBody> get(
            @Url String url,
            @HeaderMap Map<String, String> headers
    );
}
