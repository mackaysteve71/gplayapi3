package com.aurora.gplayapi.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Util {

    public static Map<String, String> parseResponse(String response) {
        final Map<String, String> keyValueMap = new HashMap<String, String>();
        final StringTokenizer stringTokenizer = new StringTokenizer(response, "\n\r");
        while (stringTokenizer.hasMoreTokens()) {
            final String[] keyValue = stringTokenizer.nextToken().split("=", 2);
            if (keyValue.length >= 2) {
                keyValueMap.put(keyValue[0], keyValue[1]);
            }
        }
        return keyValueMap;
    }
}
